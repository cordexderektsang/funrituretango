﻿using System.Collections.Generic;
using UnityEngine;
//using Vuforia;

public class PropertyGestureController : CCBaseGestureControllerVer2
{
    #region Public Variables
    [Range(0.01f, 2f)]
    public float autoFocusTime = 0.2f;
	public static GameObject currentOnTouchObject = null;
//    public CustomTrackableEventHandler customTrackableEventHandler;
    public ShuiCheunOBehaviour shuiCheunOBehaviour;
    #endregion

    #region Private Variables
    private bool isCameraOverUI = false;
    private bool isRequireFingerReset = false;
    //private bool isHitCollider = false;

    private bool isAutoFocus = true;
    private bool isAutoFocusTimer = false;
	private float autoFocusTimer = 0.0f;
	public ARGUIController1 aRGUIController1;

	//private bool userRotation;
	//private bool userScale;

    private RaycastHit hit;
    #endregion

    // Use this for initialization
    protected override void Start()
    {
        base.Start();
    }

    // Update is called once per frame
    protected override void Update()
    {
   

        base.Update();
    }

    protected override void checkTouchStateOneFinger()
    {
        if (!isRequireFingerReset)
        {
            base.checkTouchStateOneFinger();
        }
    }

    protected override void checkTouchStateTwoFinger()
    {
        base.checkTouchStateTwoFinger();

        // Don't allow user to change touch phase without re-touching screen
        if (!isRequireFingerReset)
        {
            isRequireFingerReset = true;
        }
    }

	private void changeFurnitureObject (int newIndex)
	{
		ARGUIController1.index = newIndex;
		aRGUIController1.nextItem ();
		/*GameObject nextObject = (GameObject)Instantiate (aRGUIController1.house[newIndex].thisItem);
		nextObject.transform.localPosition = ARGUIController1.thisObject.transform.localPosition;
		nextObject.transform.localEulerAngles = Vector3.zero;
		nextObject.transform.localScale = new Vector3 (0.5f, 0.5f, 0.5f);
		DestroyImmediate (ARGUIController1.thisObject);
		ARGUIController1.thisObject = (GameObject)Instantiate (aRGUIController1.house[newIndex].thisItem);*/

	}

	/// <summary>
	/// left swipe
	/// </summary>
	protected override void oneFingerSwipeLeft ()
	{
		CCScreenLogger.LogStatic ("left  : " + ARGUIController1.index, 5);
		if (ARGUIController1.index > 0) {

			changeFurnitureObject (ARGUIController1.index - 1);


		}
		// Wrap around
		else {

			changeFurnitureObject (aRGUIController1.house.Count - 1);
	

		}

		//ResetDoubleClickTimer();
	}

	/// <summary>
	/// right swipe
	/// </summary>
	protected override void oneFingerSwipeRight ()
	{	
		CCScreenLogger.LogStatic ("right  : " + ARGUIController1.index, 5);
		if (ARGUIController1.index < aRGUIController1.house.Count - 1) {

			changeFurnitureObject (ARGUIController1.index + 1);


		}else {

			changeFurnitureObject (0);

		}

		//ResetDoubleClickTimer();
	}

    #region No Finger Touch Functions
    protected override void touchStateNoFinger()
    {
        base.touchStateNoFinger();

        //mainFurnitureMovementController.clearObjectHitAndCollider();

        isCameraOverUI = false;
        isRequireFingerReset = false;
        isAutoFocus = true;

    }
    #endregion

    #region One Finger Touch Functions
    protected override void touchStateOneFingerBegan()
    {
      /*  if (UICamera.isOverUI)
        {
            isCameraOverUI = true;
        }
        else
        {
            isAutoFocusTimer = true;
        }*/
    }

    protected override void touchStateOneFingerStationary()
    {
        //if (!isCameraOverUI && (furnitureMovementTrackableHandler.isTracked || furnitureMovementTrackableHandler.isMarkerlessTracking) && Physics.Raycast(Camera.main.ScreenPointToRay(Input.mousePosition), out hit))
        //{
        //    mainFurnitureMovementController.setObjectHitCollider(hit);
        //    //isHitCollider = true;
        //}

        //isHitCollider = true;
    }

    protected override void touchStateOneFingerMoved()
    {
        //if (!isCameraOverUI)
        //{
            base.touchStateOneFingerMoved();

            //if ((furnitureMovementTrackableHandler.isTracked || furnitureMovementTrackableHandler.isMarkerlessTracking) && Physics.Raycast(Camera.main.ScreenPointToRay(Input.mousePosition), out hit))
            //{
            //    mainFurnitureMovementController.setObjectHitCollider(hit);
            //    //isHitCollider = true;
            //}

            //if (/*hitObjectCollider != null*/ mainFurnitureMovementController.isHitCollider && !isRequireFingerReset)
            //{
            //    mainFurnitureMovementController.moveObject(hit);
            //}
        //}
        //else
        //{
        //    // Change furniture texture
        //    if (radialSliderBehaviour.isPressed && radialSliderBehaviour.isRotatable/* && Input.GetMouseButton(0) && !Input.GetMouseButtonUp(0)*/)
        //    {
        //        radialSliderBehaviour.updateRadialSlider();
        //    }
        //}

        //isHitCollider = true;
    }

    protected override void touchStateOneFingerEndedCanceled()
    {
        //if (isAutoFocusTimer)
        //{
            //autoFocusTimer = 0.0f;
           // isAutoFocusTimer = false;
        //}

//		userScale = false;
//		userRotation = false;
        // Check finger action if UI was not clicked on
     //   if (!isCameraOverUI)
      //  {
            // Determine the finger action to respond to in here

            // Set autofocus
         //   if (isAutoFocus && /*!mainFurnitureMovementController.isHitCollider &&*/ !isAnyFingerMoved)
          //  {
           //     checkTouchAutofocus();
           // }
            ////else if (hitObject != null && !isAnyFingerMoved)
            //else if (!isAnyFingerMoved)
            //{
            //    mainFurnitureShowroomController.objectDeselect();
            //}
            //// Single finger swipe
            ////else if (hitObject != null)
            //else
            //{
            //    base.touchStateOneFingerEndedCanceled();
            //}
        //}
		base.touchStateOneFingerEndedCanceled();
    }

    protected override void oneFingerSwipeDown()
    {

    }

  

    protected override void oneFingerSwipeUp()
    {

    }

    protected override void oneFingerMove()
    {

    }

    private void checkTouchAutofocus()
    {
        // Set autofocus
        try
        {
            //debugLogGUIs[0] = "(Ended) auto focus";
//            CameraDevice.Instance.SetFocusMode(CameraDevice.FocusMode.FOCUS_MODE_TRIGGERAUTO);
        }
        catch
        { }
    }
    #endregion

    #region Two Finger Touch Functions
    protected override void touchStateTwoFingerBegan()
    {
/*        if (UICamera.isOverUI)
        {
            isCameraOverUI = true;
        }*/
    }

    //protected override void touchStateTwoFingerStationary()
    //{
    //    if (!isCameraOverUI && (furnitureMovementTrackableHandler.isTracked || furnitureMovementTrackableHandler.isMarkerlessTracking) && Physics.Raycast(Camera.main.ScreenPointToRay(Input.mousePosition), out hit))
    //    {
    //        mainFurnitureMovementController.setObjectHitCollider(hit);
    //        //isHitCollider = true;
    //    }
    //}

    protected override void touchStateTwoFingerMoved()
    {
        //if (!isCameraOverUI)
        //{
            base.touchStateTwoFingerMoved();

            //if ((furnitureMovementTrackableHandler.isTracked || furnitureMovementTrackableHandler.isMarkerlessTracking) && Physics.Raycast(Camera.main.ScreenPointToRay(Input.mousePosition), out hit))
            //{
            //    mainFurnitureMovementController.setObjectHitCollider(hit);
            //    //isHitCollider = true;
            //}
        //}
    }

    protected override void twoFingerSwipeDown()
    {

    }

    protected override void twoFingerSwipeLeft()
    {

    }

    protected override void twoFingerSwipeRight()
    {

    }

    protected override void twoFingerSwipeUp()
    {

    }

    protected override void twoFingerPinch()
    {
		//if (!isCameraOverUI && !userRotation)
		//if (!isCameraOverUI)
		//{
		//	userScale = true;
            //Resolution res = Screen.currentResolution;
			//float aspectRatio = res.width / res.height;
	//	CCScreenLogger.LogStatic("pinch : " + fingerPinchDistance, 0);
			//Debug.Log ("fingerPinchDistance " + fingerPinchDistance);
			shuiCheunOBehaviour.scaleObject(fingerPinchDistance /** aspectRatio * 0.001f*/);
       // }
    }

    protected override void twoFingerMove()
    {

    }

    protected override void twoFingerRotate()
    {
        // Rotation
//		if (!isCameraOverUI && !userScale)
		//if (!isCameraOverUI)
		//{
		//CCScreenLogger.LogStatic("rotatedAngle: " + rotatedAngle, 1);
			shuiCheunOBehaviour.rotateObject(rotatedAngle);
        //}
    }



    #endregion
}
